# Requirements

* Python 3.8
* Python tkinter
* ADS1115 16-bit ADC - 4 Channel
* Pandas
* MatPlotLib

```
sudo apt install -y build-essential python3 python-is-python3 python3-tk
```

# Installing Adafruits ADS1115 Lib

```
sudo pip3 install adafruit-circuitpython-ads1x15
```

# Installing pip3 dependencies

```
pip3 install pandas matplotlib
```
