#!/usr/bin/env python
#
# Read and display temperature values with a graph.
#
import time
import tkinter as tk
from tkinter import ttk
from tkinter import font
from typing import List
from pandas import DataFrame
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure
import Temp as T


class Application(tk.Frame):
    def __init__(self, master=None) -> tk.Tk():
        """Initializes the application as a root Tk object"""

        super().__init__(master)
        self.master = master
        self.pack()
        
        # Initialize temperature reading.
        self.temp = T.Temp()

        # Setup temperature plot.
        self.temp_chart_epoch_s = time.time()
        self.temp_fig = Figure(figsize=(12, 6), dpi=100)
        self.temp_ax = self.temp_fig.add_subplot(1, 1, 1)
        self.data_time_s = []
        self.data_temp = []
        for i in range(0, 4):
            self.data_time_s.append([])
            self.data_temp.append([])
        self.read_graph = False

        # Setup tk variables.
        self.var_channel_name_arr = []
        self.var_channel_sampling_arr = []
        self.var_channel_temp_arr = []
        for i in range(0, 4):
            self.var_channel_name_arr.append(tk.StringVar(master, "Channel " + str(i)))
            self.var_channel_sampling_arr.append(tk.IntVar(master, 1000))
            self.var_channel_temp_arr.append(tk.DoubleVar(master, 0.0))

        # Add notebook widget to root.
        self.tab_control = ttk.Notebook(self, width=1024)
        self.tab_control.grid()

        # Setup tabs.
        self.setup_temp_chart_tab()
        self.setup_channels_tab()

        # Setup periodic reads for reading temperature.
        for channel in range(0, 4):
            self.read_temp_loop(channel)


    def read_temp(self, channel):
        """Retrieve temperature from ADS1115"""

        if channel < 0 or channel > 3:
            return
        self.var_channel_temp_arr[channel].set(round(self.temp.get_temp(channel), 2))
        self.add_temp_to_plot(channel, self.var_channel_temp_arr[channel].get())

    def read_temp_loop(self, channel):
        """Read the four channels from the ADS1115 on an interval"""

        self.read_temp(channel)
        interval_ms = self.var_channel_sampling_arr[channel].get()

        # If the interval value is less than 1000ms, let's ensure it's no less than that.
        if interval_ms < 1000:
            interval_ms = 1000
        self.after(interval_ms, self.read_temp_loop, channel)

    def draw_temp_chart_loop(self, chart):
        """Redraw temp chart in a loop"""
        if self.read_graph:
            self.temp_ax.legend(loc="upper right")
            self.temp_ax.set_ylabel("Celsius")
            self.temp_ax.set_xlabel("Time (s)")
            self.temp_ax.set_title("Temperature over Time")
            chart.draw()
        self.after(1000, self.draw_temp_chart_loop, chart)

    def add_temp_to_plot(self, channel, temp):
        """Add temperature to plot"""

        # Skip adding to plot if we are not reading to graph.
        if not self.read_graph:
            return

        # Add temperature with timestamp.
        self.data_time_s[channel].append(time.time() - self.temp_chart_epoch_s)
        self.data_temp[channel].append(temp)

        # Draw temperature to timestamp.
        self.temp_ax.clear()
        for i in range(0, 4):
            channel_label = self.var_channel_name_arr[i].get() + ": " + str(self.var_channel_temp_arr[i].get()) + " C"
            self.temp_ax.plot(self.data_time_s[i], self.data_temp[i], label=channel_label)

    def clear_graph(self):
        """Clear the graph"""

        for i in range(0, 4):
            self.data_time_s[i].clear()
            self.data_temp[i].clear()
        self.temp_ax.clear()


    def set_read_graph(self, enable):
        """Enable reading the graph"""

        self.read_graph = enable
        if (enable):
            self.clear_graph()
            self.temp_chart_epoch_s = time.time()


    def setup_channels_tab(self):
        """Setup a tab to display all four channels on the ADS1115

        Setup the following:
        * Tab in the tab control called "Channels"
        * Four entries to rename the channel
        * Four spinboxes to change sampling interval (1-255 seconds)
        * Four entries to display channel temperature in Celsius
        * Chart to display temperature
        """

        # Add new tab to tab control for displaying the channels.
        tab_channels = ttk.Frame(self.tab_control)
        self.tab_control.add(tab_channels, text="Channels")

        # Create GUI elements for each channel.
        for i in range(0, 4):
            # Add entry for renaming channel.
            label_channel_name_header = ttk.Label(tab_channels, text="Channel Name")
            label_channel_name_header.grid(row=0,
                                           column=0,
                                           padx=2,
                                           pady=2)
            entry_channel_name = ttk.Entry(tab_channels,
                                           textvariable=self.var_channel_name_arr[i],
                                           width=20,
                                           justify="center")
            entry_channel_name.grid(row=i + 1,
                                    column=0,
                                    padx=2,
                                    pady=2)

            # Add sampling rate entry.
            label_channel_sampling_header = ttk.Label(tab_channels, text="Sampling (ms)")
            label_channel_sampling_header.grid(row=0,
                                               column=2,
                                               padx=2,
                                               pady=2)
            spinbox_channel_sampling = ttk.Spinbox(tab_channels,
                                                   textvariable=self.var_channel_sampling_arr[i],
                                                   from_=100,
                                                   to=10000,
                                                   width=4,
                                                   justify="center")
            spinbox_channel_sampling.grid(row=i + 1,
                                          column=2,
                                          padx=2,
                                          pady=2)

            # Add channel temperature display in Celsius.
            label_channel_temp_header = ttk.Label(tab_channels, text="Temperature (C)")
            label_channel_temp_header.grid(row=0,
                                           column=3,
                                           padx=2,
                                           pady=2)
            entry_temp = ttk.Entry(tab_channels,
                                   textvariable=self.var_channel_temp_arr[i],
                                   state="readonly",
                                   width=10,
                                   justify="center")
            entry_temp.grid(row=i + 1,
                            column=3,
                            padx=2,
                            pady=2)

    def setup_temp_chart_tab(self):
        """Setup tab to display temperature chart"""

        # Add about tab to tab control.
        tab_chart = ttk.Frame(self.tab_control)
        self.tab_control.add(tab_chart, text="Chart")

        # Add chart.
        chart = FigureCanvasTkAgg(self.temp_fig, tab_chart)
        toolbar = NavigationToolbar2Tk(chart, tab_chart)
        toolbar.update()
        chart.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        self.draw_temp_chart_loop(chart)

        # Add Start button.
        start_button = ttk.Button(tab_chart, text="START", command=lambda: self.set_read_graph(True))
        start_button.pack(side=tk.LEFT, expand=True)

        # Add Stop button.
        stop_button = ttk.Button(tab_chart, text="STOP", command=lambda: self.set_read_graph(False))
        stop_button.pack(side=tk.LEFT, expand=True)


def main():
    """Main entry to the application to read the ADS1115 channels"""

    root = tk.Tk()
    root.title("RasPiThermo")
    root.geometry("1280x720")
    app = Application(master=root)
    app.mainloop()


if __name__ == "__main__":
    main()
