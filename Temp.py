# Initialize ADS1x15 board for reading in temperature value.
# This also allows for an optional test that does not require an ADS1x15 for
# reading in temperature values.
from enum import Enum
class ADS1X15(Enum):
    ADS1015 = 0
    ADS1115 = 1
ADS_BOARD = ADS1X15.ADS1015

try:
    import board
except:
    print("Warning! Unable to import python3 package 'board' for reading the I2C bus")
import busio
if ADS_BOARD == ADS1X15.ADS1015:
    import adafruit_ads1x15.ads1015 as ADS
else:
    import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

def analog_to_c(voltage, max_voltage=3.3, ro=10000.0, to=25.0, beta=3950.0):
    """Convert analog voltage to temperature Centrigrade"""
    import math
    if voltage <=0 or voltage >= max_voltage:
        print("We are below 0V or above max_voltage!")
        return -9999.9 # Indicate that we are below or above voltage with a no-read output.
    r = ro / ((max_voltage / voltage) - 1) # Convert voltage to resistance
    temp_c = math.log(r / ro) / beta       # log(R/Ro) / beta
    temp_c += 1.0 / (to + 273.15)          # log(R/Ro) / beta + 1/To
    temp_c = (1.0 / temp_c) - 273.15       # Invert, convert to C
    return temp_c

class Temp():
    def __init__(self):
        """Initialize an ADS1x15 board"""
        try:
            i2c = busio.I2C(board.SCL, board.SDA)
            if ADS_BOARD == ADS1X15.ADS1015:
                # Attempt to initialize ADS1015.
                self.ads = ADS.ADS1015(i2c)
            else:
                # Attempt to initialize ADS1115.
                self.ads = ADS.ADS1115(i2c)
        except:
            print("Unable to initialize ADS1x15 board")
        
    def get_analog(self, pin):
        """Get Analog value (volts) from pin"""
        if pin == 0:
            return AnalogIn(self.ads, ADS.P0).voltage
        elif pin == 1:
            return AnalogIn(self.ads, ADS.P1).voltage
        elif pin == 2:
            return AnalogIn(self.ads, ADS.P2).voltage
        elif pin == 3:
            return AnalogIn(self.ads, ADS.P3).voltage
        else:
            return 0.0
        
    def get_temp(self, pin):
        """Return temperature for the pin"""
        return analog_to_c(self.get_analog(pin))

class TempTest(Temp):
    def __init__(self):
        print("TempTest!")
    
    def get_analog(self, pin):
        """Return random analog values"""
        import random
        return random.random() * 3.3