#!/bin/bash

APP_UI_OVERRIDE_PATH=/usr/share/raspi-ui-overrides/applications/
DEFAULT_LXDE_PANEL_PATH=/etc/xdg/lxpanel/LXDE-pi/panels/panel
LXDE_PANEL_PATH=/home/pi/.config/lxpanel/LXDE-pi/panels/panel

# Install packages.
echo '=== Updating and upgrading packages'
sudo apt update
sudo apt upgrade -y
echo '=== Installing build-essentials and core python3 packages'
sudo apt install -y build-essential python3 python-is-python3 python3-tk

# Install ASD1x15 python3 lib.
echo '=== Installing ADS1x15 python lib'
sudo pip3 install adafruit-circuitpython-ads1x15

# Install MatPlotLib python3 lib.
echo '=== Installing pandas and matplotlib'
sudo pip3 install pandas matplotlib

# Install Virtual Keyboard
echo '=== Attempting to install matchbox keyboard'
sudo apt install -y matchbox-keyboard
echo '=== Moving toggle-keyboard.sh to /usr/bin'
sudo cp toggle-keyboard.sh /usr/bin/
echo '=== Moving toggle-keyboard.desktop to $APP_UI_OVERRIDE_PATH'
sudo cp toggle-keyboard.desktop $APP_UI_OVERRIDE_PATH
if [ ! -f $LXDE_PANEL_PATH ]; then
	echo '=== Did not find $LXDE_PANEL_PATH'
	echo '=== Copying default configuration from $DEFAULT_LXDE_PANEL_PATH to $LXDE_PANEL_PATH'
	sudo cp $DEFAULT_LXDE_PANEL_PATH $LXDE_PANEL_PATH
fi
if grep -Fxq 'toggle-keyboard.desktop' $LXDE_PANEL_PATH; then
	echo '=== $LXDE_PANEL_PATH already has toggle-keyboard plugin installed.'
else
	echo '=== Appending keyboard panel toggle plugin to $LXDE_PANEL_PATH'
	cat panel_append | sudo tee -a $LXDE_PANEL_PATH >> /dev/null
fi
